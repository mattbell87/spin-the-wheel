# Spin the wheel

## Installing

Ensure you have GIT and NPM/NodeJS installed.

```sh
git clone <repo url>
cd spin-the-wheel
npm install
```

## Running
To run the dev server
```sh
npm start
```
To run the multiplayer server:
```sh
npm run server
```

## Building
To build the static site:
```sh
npm run build
```
You can now upload the contents of the build folder.

## Testing
```sh
npm test
```

## Documentation

You can see the documentation at http://mattbell.com.au:3000/docs/.

You can also `npm run jsdoc` and open up the doc folder.

## Live demo

http://mattbell.com.au:3000/

## Development

See [the development plan](Development-Plan.md).