import React, { Component } from 'react';
import ReactSVG from 'react-svg';
import ReactDOM from 'react-dom';
import WheelIMG from './img/wheel.svg';
import ArrowIMG from './img/arrow.svg';
import './Wheel.css';
import Velocity from './Velocity';
import Vector2D from './Vector2D';
import { setTimeout } from 'timers';

/**
 * The wheel component of the game. Handles spinning and scoring.
 */
class Wheel extends Component {
  constructor() {
    super();
    this.velocity = new Velocity();
    this.velocity.maximum = 20;
    this.rotation = 0;
    this.offsetRotation = 0;
    this.rotationStyle = {
      transform: "rotate(0deg)"
    };
    this.values = [10,1,8,6,5,7,9,3,4,2];
  }

  /**
   * When react renders set up the velocity for the wheel
   */
  componentWillMount() {
    this.velocity.onUpdate(()=>{
        this.setRotation(this.rotation + this.velocity.value);
    });
    this.lock();
  }

  /**
   * Reset the wheel
   */
  reset(){
    var lockOnState = [
      Wheel.moveState.spinning,
      Wheel.moveState.holding
    ];
    if (lockOnState.indexOf(this.moveState) !== -1) {
      this.lock();
    }
    this.setRotation(0);
  }

  /**
   * Unlock the wheel
   */
  unlock() {
    this.moveState = Wheel.moveState.ready;
    this.setState({
      status: 'Give it a spin!'
    });
  }

  /**
   * Lock the wheel
   */
  lock() {
    this.moveState = Wheel.moveState.locked;
    this.setState({
      status: 'Waiting...'
    });
  }

  /**
   * Get the current rotation and return the matching value
   */
  angleToValue() {
    return this.values[Math.floor((360-this.rotation)/36)];
  }

  /**
   * Get the rect of the wheel and use Vector2D to calculate the look rotation
   * @param {Vector2D} posVector The position to look at (the mouse) 
   */
  getLookRotation(posVector) {
    var rect = ReactDOM.findDOMNode(this.wheel).getBoundingClientRect();
    var origin = new Vector2D(rect.left + rect.width / 2, rect.top + rect.height / 2);
    return origin.getLookRotation(posVector);
  }
  
  /**
   * Event listener for when input is moved
   */
  move = (event) => {
    if (this.moveState === Wheel.moveState.holding) {
      //Capture touch events
      if (event.touches && event.touches.length > 0) {
        event = event.touches[0];
      }

      var mouse = new Vector2D(event.clientX, event.clientY);
      this.setRotation(this.getLookRotation(mouse) - this.offsetRotation);
      this.velocity.add(this.rotation);
    }
  }

  /**
   * Event listener for when button is held down
   */
  hold = (event) => {
    //Stop dragging
    event.preventDefault();

    //Capture touch events
    if (event.touches && event.touches.length > 0) {
      event = event.touches[0];
    }

    //Set the state
    if (this.moveState === Wheel.moveState.ready) {
      this.moveState = Wheel.moveState.holding;
      this.offsetRotation = this.getLookRotation(new Vector2D(event.clientX, event.clientY)) - this.rotation;
    }
  }

  /**
   * Event listener for when button is let go
   */
  letGo = () => {
    if (this.velocity.getSpeed() > 2) {
      //Set the state
      if (this.moveState === Wheel.moveState.holding) {
        //Spin the wheel, score when stopped
        this.moveState = Wheel.moveState.spinning;
        this.spin(()=>{
          this.stopped();
        });
        this.updateParent();
      }
    } else {
      if (this.moveState !== Wheel.moveState.locked) {
      this.moveState = Wheel.moveState.ready;
        this.setState({
          status: 'Spin it harder!'
        });
      }
    }
    }

  /**
   * Spin the wheel
   */
  spin(onStop) {
    this.velocity.friction(2, ()=>{
      if (typeof onStop === 'function') {
        this.moveState = Wheel.moveState.locked;
        onStop();
  }
    });
  }

  /**
   * Called when the wheel has stopped moving
   */
  stopped() {
    var score = this.angleToValue();
    this.updateParent();
    this.setState({
      score: score,
      status: "You scored " + score
    });
    if (typeof this.props.onScored === 'function') {
      this.props.onScored(score);
    } 
  }

  /**
   * Set the rotation of the wheel
   * @param {float} degrees 
   */
  setRotation(degrees) {
    if (degrees < 0) {
      degrees += 360;
    } else if (degrees > 360) {
      degrees -= 360;
    }
    this.rotation = degrees;
    //Change the state so that rotations are remembered when rerendering
    this.rotationStyle = {
      transform: "rotate("+this.rotation+"deg)"
    }
    //Instantly update the rotation
    var wheel = ReactDOM.findDOMNode(this.wheel).querySelector('svg');
    wheel.style.transform = this.rotationStyle.transform;
  }

  /**
   * Send updates to the parent when spinning (for multiplayer)
   */
  updateParent() {
    if (typeof this.props.onUpdate === 'function') {
      this.props.onUpdate({
        rotation: this.rotation,
        velocity: this.velocity.value
      });
    }
    if (this.velocity.getSpeed() === 0) {
      return;
    }
    setTimeout(()=>{this.updateParent()},500);
  }

  componentDidCatch(error, info) {
    this.setState({error:error});
  }

  /**
   * Render the wheel
   */
  render() {
    if (this.state.error) {
      return (
        <div>{this.state.error.toString()}</div>
      )
    }
    return (
      <div className="wheel-area"  
        onMouseMove={this.move} 
        onMouseDown={this.hold} 
        onMouseUp={this.letGo}
        onTouchStart={this.hold}
        onTouchMove={(event) => {this.move(event)}}
        onTouchEnd={this.letGo}
        onTouchCancel={this.letGo}
      >
        <p className="wheel-state">{this.state.status}</p>
        <ReactSVG
          className="arrow"
          path={ArrowIMG}
        />
        <ReactSVG
          className="wheel"
          path={WheelIMG}
          ref={(elem) => this.wheel = elem}
          style={this.rotationStyle}
        />
      </div>
    );
  }
}

/**
 * State of the wheel
 */
Wheel.moveState = {
  ready: 0,
  holding: 1,
  spinning: 2,
  locked: 3
}

export default Wheel;
