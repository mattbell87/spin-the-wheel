/**
 * Stores x and y coordinates and performs calculations against other Vector2D's  
 */
class Vector2D {
    constructor(x,y) {
        this.x = x || 0;
        this.y = y || 0;
        if (isNaN(this.x) || isNaN(this.y)){
            throw new Error("Vector must be numeric values (X:"+this.x+",Y:"+this.y+")");
        }
    }

    /**
     * Calulate distance from another vector
     * @param {Vector2D} otherVector 
     * @returns distance as a float
     */
    distance(otherVector) {
        return Math.sqrt(Math.pow(this.x - otherVector.x, 2) + Math.pow(this.y - otherVector.y, 2));
    }
    
    /**
     * Calculate the look rotation of another vector
     * @param {Vector2D} otherVector 
     * @returns angle in degrees
     */
    getLookRotation(otherVector) {
        var angle = Math.atan2(this.y - otherVector.y, this.x - otherVector.x);
        return (angle * (180/Math.PI)) + 90;
    }
}

export default Vector2D;
