import Velocity from './Velocity';

test('Empty Velocity can have angles added', ()=>{
    var velocity = new Velocity();
    velocity.add(10);
    velocity.add(20);
    expect(velocity.angles.length).toEqual(2);
});

test('Velocity can add and drop angles', ()=>{
    var firstAngle = 3;
    var testAngle = 4;

    //Add some with constructor
    var velocity = new Velocity([firstAngle,testAngle]);
    
    //Add some with add()
    velocity.add(testAngle);

    //First will get dropped here
    velocity.add(testAngle);
    expect(velocity.angles.length).toEqual(3);

    //First should now be one of "testAngle", not "firstAngle"
    expect(velocity.angles[0]).toEqual(4);

    //The value should also be 0
    expect(velocity.value).toEqual(0);
});

test('Velocity calculates correctly', ()=>{
    var vel = new Velocity([20,30,60]);
    expect(vel.value).toEqual(20);
});

test('Velocity clears correctly', ()=>{
    var vel = new Velocity([20,30,60]);
    vel.clear();
    expect(vel.angles.length).toEqual(0);
});