import React, { Component } from 'react';
import './Game.css';
import Wheel from './Wheel.js';
import ModalWelcome from './modals/ModalWelcome';
import ModalError from './modals/ModalError';
import ModalEnd from './modals/ModalEnd';
import ModalServerConnect from './modals/ModalServerConnect';
import ModalLobby from './modals/ModalLobby';
import ModalWaiting from './modals/ModalWaiting';
import socketIOClient from "socket.io-client";

/**
 * This is the main component that controls the client side of the game
 */
class Game extends Component {

  /**
   * Set up the initial state of the game
   */
  componentWillMount() {
    var serverUri = 
      window.location.protocol + "//" +
      window.location.hostname + ":3001";
    this.setState({
      name: "",
      defaultUri: serverUri
    });
    this.reset();
  }

  /**
   * Listen for when the player has scored
   */
  scored = (score) => {
    //Add score, take a turn
    this.setState({
      score: this.state.score+score,
      turnsLeft: this.state.turnsLeft-1
    });
    if (this.state.multiplayer && this.state.myturn) {
      this.wheel.lock();
      this.socket.emit('scored', score);
      this.setState({
        myturn: false
      });
    } else {
      this.setState({
        message: "You scored "+score+" points"
      });
      this.checkTurns();
    }
  }

  /**
   * Start a single player game
   */
  startSinglePlayer = () => {
    this.setState({
      score: 0,
      turnsLeft: 5,
      playing: true
    });
    this.wheel.unlock();
  }

  /**
   * Start a Multi player game
   */
  startMultiPlayer = () => {
    this.setState({
      score: 0,
      turnsLeft: 5,
      playing: true,
      serverScreen: true
    });
    this.wheel.lock();
  }

  /**
   * Connect to the specified server
   * @param {string} playerName
   * @param {string} serverUri Server to connect to
   */
  connectToServer = (playerName, serverUri) => {
    this.setState({
      serverScreen: false,
      waiting: true
    });
    this.socket = socketIOClient(serverUri);
    this.socket.on('connectedsuccess', () => {
      this.socket.emit('join', playerName);
    });
    this.socket.on('joined', (isHost) => {
      this.setState({
        waiting: false,
        pregame: true,
        isHost: isHost,
        name: playerName,
        defaultUri: serverUri
      });
    });
    this.socket.on('connect_error', () => {
      this.setState({
        error: "Could not connect to server",
        waiting: false
      });
      this.socket.disconnect();
    });
    this.socket.on('gamestate', (gamestate) => {
      this.setState({
        gamestate: gamestate
      });
    });
    this.socket.on('message', (message) => {
      this.setState({
        message: message
      });
    });
    this.socket.on('newturn', () => {
      this.wheel.reset();
    });
    this.socket.on('yourturn', () => {
      this.setState({
        myturn: true,
        message: "Your turn"
      });
      this.wheel.unlock();
    });
    this.socket.on('gamestarted', () => {
      this.setState({
        pregame: false,
        turnsLeft: this.state.gamestate.rounds,
        multiplayer: true
      });
    });
    this.socket.on('updateWheel', (wheelState) => {
      this.wheel.velocity.set(wheelState.velocity);
      this.wheel.setRotation(wheelState.rotation);
      if (wheelState.velocity !== 0) {
        this.wheel.spin();
      }    
    });
    this.socket.on('gameEnded', (name, score) => {
      this.setState({
        winner: name,
        winnerScore: score,
        endGameScreen: true
      });
    });
    this.socket.on('youwon', ()=>{this.setState({youwon:true})});
  }

  wheelUpdated = (wheelstate) => {
    if (this.state.multiplayer && this.state.myturn) {
      this.socket.emit('wheelUpdate', wheelstate);
    }
  }

  /**
   * Resets the game state
   */
  reset = () => {
    this.setState({
      serverScreen: false,
      waiting: false,
      playing: false,
      multiplayer: false,
      isHost: false,
      youwon: false,
      score: 0,
      turnsLeft : 0,
      endGameScreen: false,
      message: "",
      winner: null,
      winnerScore: 0,
      pregame: false,
      error: null
    });
    if (this.wheel) {
      this.wheel.reset();
    }
    if (this.socket && this.socket.connected) {
      this.socket.disconnect();
    }
  }

  /**
   * Checks the turns left for single player
   */
  checkTurns() {
    if (this.state.turnsLeft === 0) {
      this.wheel.lock();
      this.setState({
        winner: null,
        endGameScreen: true
      });
    } else {
      this.wheel.unlock();
    }
  }

  /**
   * Render the game
   */
  render() {
    return (
      <div className="App">
      
        <header className="App-header">
          <h1 className="App-title">Spin The Wheel</h1>
        </header>

        <p className="score">
          Score {this.state.score}. Turns left: {this.state.turnsLeft}.
        </p>
        <p className="message">{this.state.message}</p>

        <ModalError error={this.state.error} onClose={()=>{this.reset()}}/>

        <Wheel onScored={this.scored} ref={(obj) => {this.wheel = obj;}} onUpdate={this.wheelUpdated} />

        <ModalWelcome 
          open={!this.state.playing} 
          spClicked={this.startSinglePlayer} 
          mpClicked={this.startMultiPlayer} />
        <ModalEnd 
          open={this.state.endGameScreen} 
          onClose={this.reset} 
          score={this.state.score} 
          winner={this.state.winner} 
          winnerScore={this.state.winnerScore}
          youwon={this.state.youwon} />
        <ModalServerConnect 
          open={this.state.serverScreen} 
          onClose={this.reset} 
          connectClicked={this.connectToServer}
          defaultName={this.state.name}
          defaultUri={this.state.defaultUri}
          ref={(obj)=>{this.serverModal = obj}} />
        <ModalLobby
          open={this.state.pregame}
          gamestate={this.state.gamestate}
          isHost={this.state.isHost}
          ref={(obj)=>{this.pregameModal = obj}} 
          startClicked={()=>{this.socket.emit('startgame')}} />
        

        <ModalWaiting waiting={this.state.waiting} />
      </div>
    );
  }
}

export default Game;
