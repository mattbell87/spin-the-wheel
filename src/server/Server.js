/**
 * Start up the server
 */

var server = require('http').createServer();
var io = require('socket.io')(server);
var Player = require("./Player");
var GameServer = require("./GameServer");

var game = new GameServer();

io.on('connection', function(client){
    game.addPlayer(new Player(client, game));
});

server.listen(3001);
console.log("Server listening on port 3001");