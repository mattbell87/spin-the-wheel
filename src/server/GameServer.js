/**
 * Represents a server session
 */
class GameServer {
    constructor() {
        /**
         * Array of players
         * @type {Player[]}
         */
        this.players = [];

        /**
         * Array for the turns, each item is a reference to a player
         * @type {Player[]}
         */
        this.turns = [];
         
        /**
         * Number of rounds
         * @type {int}
         */
        this.rounds = 3;

        /**
         * Current Turn
         * @type {int}
         */
        this.currentTurn = 0;

        /**
         * Has the game started yet?
         * @type {boolean}
         */
        this.hasStarted = false;
    }

    /**
     * Add a player to the game
     * @param {Player} player 
     */
    addPlayer(player) {
        if (this.players.length == 0) {
            player.isHost = true;
        }
        this.players.push(player);
    }

    /**
     * Send the player list and game state to the connected players
     */
    updateGameState() {
        var playerinfo = [];
        for (var i in this.players) {
            var player = this.players[i];
            playerinfo.push({
                name: player.name,
                score: player.score,
                turnsLeft: player.turnsLeft()
            });
        }
        this.emitAll('gamestate', {
            rounds: this.rounds,
            hasStarted: this.hasStarted,
            totalTurnsLeft: this.turns.length,
            players: playerinfo
        });
    }
    /**
     * Remove a player from the game
     * @param {Player} player 
     */
    removePlayer(player) {
        if (player.client.connected) {
            player.client.disconnect();
        }
        if (this.players.indexOf(player) === -1) {
            return;
        }
        this.players.splice(this.players.indexOf(player), 1);
        if (player == this.currentPlayer) {
            this.nextTurn();
            if (player == this.currentPlayer) {
                //Player was the only player
                this.end();
                return;
            }
        }
        while (this.turns.indexOf(player) !== -1) {
            this.turns.splice(this.turns.indexOf(player), 1);
        }
        
        player = null;
        this.updateGameState();
    }

    /**
     * Send a message to all players
     * @param {string} message 
     */
    messageAll(message) {
        this.emitAll("message", message);
        console.log(message);
    }

    /**
     * Emit an event to all players
     * @param {string} event 
     * @param {...args} args
     */
    emitAll(event, ...args) {
        for (var i in this.players) {
            this.players[i].client.emit(event, ...args);
        }
    }

    /**
     * Start the game
     */
    start() {
        //Build the turns
        this.turns = [];
        for (var i = 0; i < this.rounds; i++) {
            for (var p in this.players) {
                this.turns.push(this.players[p]);
            }
        }
        this.messageAll("The game has started");
        this.emitAll('gamestarted');

        this.nextTurn();
    }

    /**
     * Get the next turn from the turn array or end the game if there
     * are no turns left
     */
    nextTurn() {
        if (this.turns.length == 0) {
            this.end();
            return;
        }
        this.currentPlayer = this.turns.shift();

        this.currentPlayer.turn();
    }

    /**
     * End the game
     */
    end() {
        var topScore = 0;
        var topPlayer = null;
        for (var p in this.players) {
            if (this.players[p].score > topScore) {
                topPlayer = this.players[p];
                topScore = topPlayer.score;
            }
        }

        this.messageAll('Game Ended');
        if (topPlayer !== null) {
            this.emitAll('gameEnded', topPlayer.name, topScore);
            topPlayer.client.emit('youwon');
        }

        setTimeout(() => {
            while (this.players.length > 0) {
                var player = this.players.shift();
                player.client.disconnect();
            }
        }, 2000);
    }
}

module.exports = GameServer;