/**
 * A player connected to the server
 */
class Player {
    /**
     * @param {Socket} client 
     * @param {GameServer} game
     */
    constructor(client, game) {
        /**
         * @type {Socket}
         */
        this.client = client;
        /**
         * @type {GameServer}
         */
        this.game = game;
        this.score = 0;
        this.isHost = false;
        this.connected();
    }

    /**
     * Listener for when the player has connected
     */
    connected() {
        this.client.on('join', (name) => {this.join(name)});
        this.client.on('disconnect', () => {this.onDisconnect()});
        this.client.emit('connectedsuccess');
    }

    /**
     * Listener for when the player has joined the session
     */
    join(name) {
        this.name = name;
        this.game.messageAll(this.name + " has joined the game");
        this.client.emit('joined', this.isHost);
        this.game.updateGameState();
        this.joined = true;
        this.client.on('startgame', ()=>{
            if (this.isHost) {
                this.game.start();
            }
        });
        this.client.on('scored', (points)=>{this.scored(points)});
        this.client.on('wheelUpdate', (wheelState)=>{
            if (this.game.currentPlayer == this) {
                for (var i in this.game.players) {
                    var player = this.game.players[i];
                    //Dont send the update to the player that's spinning
                    if (player === this) {
                        continue;
                    }
                    player.client.emit('updateWheel', wheelState);
                }
            }
        });
    }

    /**
     * Listener for when the player disconnects
     */
    onDisconnect() {
        if (this.joined) {
            this.game.messageAll(this.name + " has left the game");
            this.game.removePlayer(this);
        } else {
            console.warn('Unjoined client disconnected');
        }
    }

    /**
     * When it's this players turn
     */
    turn() {
        this.game.messageAll(this.name + "'s turn");
        this.game.emitAll('newturn');
        this.client.emit('yourturn');
    }

    /**
     * Get the number of turns this player has left
     */
    turnsLeft() {
        var turns = 0;
        for (var i in this.game.turns) {
            if (this.game.turns[i] == this) {
                turns++;
            }
        }
        return turns;
    }

    /**
     * When the player scores points
     * @param {int} points 
     */
    scored(points) {
        if (this.game.currentPlayer == this) {
            this.score += points;
            this.game.messageAll(this.name + " scored " + points + " points");
            this.game.updateGameState();
            setTimeout(() => {
                this.game.nextTurn();
            }, 2000);
        }
    }

}

module.exports = Player;
