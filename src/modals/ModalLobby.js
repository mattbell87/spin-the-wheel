import React from 'react';
import Modal from 'react-responsive-modal';

/**
 * Lobby modal
 */
class ModalLobby extends React.Component {

  /**
   * Render the bottom panel (different if you are the host)
   */
  bottomPanel = () => {
    if (this.props.isHost) {
      return <p> You are the host <br />
        <button onClick={this.props.startClicked}>Start the game</button>
      </p>
    } else {
      return <p>Please wait for the host to start the game</p>
    }
  }

  /**
   * Render the list of players
   */
  players = () => {
    if (!this.props.gamestate || !this.props.gamestate.players) {
      return null;
    }
    var players = this.props.gamestate.players;
    var i = 0;
    var listItems = players.map((player)=>{
      i++;
      return <li key={i}>{player.name}</li>
    });
    return <ol>{listItems}</ol>;
  }

  onCloseModal = () => {
    if (typeof this.props.onClose === 'function') {
        this.props.onClose();
    }
  };

  render() {
    var BottomPanel = this.bottomPanel;
    var Players = this.players;

    return (
      <Modal open={this.props.open === true} onClose={this.onCloseModal}>
        <h2>Lobby</h2>
        <h3>Players:</h3>
        <Players />
        <BottomPanel />
      </Modal>
    );
  }
}

export default ModalLobby;