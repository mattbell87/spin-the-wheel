import React from 'react';
import Modal from 'react-responsive-modal';

/**
 * Connect to server modal
 */
class ModalServerConnect extends React.Component {
  state = {
    open: false,
  };

  onCloseModal = () => {
    if (typeof this.props.onClose === 'function') {
        this.props.onClose();
    }
  };

  onConnectClicked = () => {
    if (typeof this.props.connectClicked === 'function') {
      var name = this.nameField.value.trim();
      if (name.length === 0) {
        name = "Unnamed Player";
      }
      var server = this.serverField.value.trim();
      if (server.length === 0) {
        server = this.props.defaultUri;
      }
      this.props.connectClicked(name, server);
    }
  }

  render() {
    return (
      <Modal open={this.props.open === true} onClose={this.onCloseModal} little>
        <h2>Connect to a server</h2>
        <p><label htmlFor="name">Name</label> <input id="name" type="text" defaultValue={this.props.defaultName} placeholder="Your Name" ref={(obj)=>{this.nameField = obj}} /></p>
        <p><label htmlFor="serveruri">Server</label> <input id="serveruri" type="text" defaultValue={this.props.defaultUri} ref={(obj)=>{this.serverField = obj}} /></p>
        <p><button onClick={this.onConnectClicked}>Connect</button></p>
      </Modal>
    );
  }
}

export default ModalServerConnect;