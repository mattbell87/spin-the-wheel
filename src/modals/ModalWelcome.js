import React from 'react';
import Modal from 'react-responsive-modal';

/**
 * Single or Multiplayer selection modal
 */
class ModalWelcome extends React.Component {
  state = {
    open: false,
  };

  onCloseModal = () => {
    if (typeof this.props.onClose === 'function') {
        this.props.onClose();
    }
  };

  render() {
    return (
        <Modal open={this.props.open === true} onClose={this.onCloseModal} little>
          <h2>Welcome to spin the wheel</h2>
          <p><button onClick={this.props.spClicked}>Single Player</button></p>
          <p><button onClick={this.props.mpClicked}>Multi Player</button></p>
        </Modal>
    );
  }
}

export default ModalWelcome;