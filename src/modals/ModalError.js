import React from 'react';
import Modal from 'react-responsive-modal';

/**
 * Modal that diplays errors
 */
class ModalError extends React.Component {
  onCloseModal = () => {
    this.setState({ error: null });
    if (typeof this.props.onClose === 'function') {
        this.props.onClose();
    }
  };

  render() {
    return (
        <Modal open={this.props.error !== null} onClose={this.onCloseModal} little>
            <h2>Error</h2>
            <p>{this.props.error}</p>
            <button onClick={this.onCloseModal}>Close</button>
        </Modal>
    );
  }
}

export default ModalError;