import React from 'react';
import Modal from 'react-responsive-modal';

/**
 * End of game modal
 */
class ModalEnd extends React.Component {
  onCloseModal = () => {
    this.setState({ open: false });
    if (typeof this.props.onClose === 'function') {
        this.props.onClose();
    }
  };

  render() {
    const Header = () => {
      //Single player
      if (this.props.winner === null) {
        return "Well done!";
      }
      //Multi player
      if (this.props.youwon === true) {
        return "You won the game!";
      }
      return this.props.winner + " won the game";
    }
    const Score = () => {
      //Single player
      if (this.props.winner === null) {
        return <p>You scored <strong className="modalScore">{this.props.score}</strong> points.</p>
      }
      //Multi player
      if (this.props.youwon === true) {
        return <p>Your score topped everyone else at <strong className="modalScore">{this.props.score}</strong> points.</p>
      }
      return <p>
          They scored <strong className="modalScore">{this.props.winnerScore}</strong> points.
          <br />
          Your score was <strong className="modalScore">{this.props.score}</strong> points.
        </p>
    }
    return (
        <Modal open={this.props.open === true} onClose={this.onCloseModal} little>
            <h2><Header /></h2>
            <Score />
            <button onClick={this.onCloseModal}>Start Over</button>
        </Modal>
    );
  }
}

export default ModalEnd;