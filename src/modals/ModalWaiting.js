import React from 'react';
import Modal from 'react-responsive-modal';

/**
 * Modal that shows when the player is waiting for the app to do something
 */
class ModalWaiting extends React.Component {
  state = {
    open: false,
  };

  onCloseModal = () => {
    this.setState({ open: false });
    if (typeof this.props.onClose === 'function') {
        this.props.onClose();
    }
  };

  render() {
    return (
      <Modal open={this.props.waiting === true} onClose={this.onCloseModal}>
        <h2>Please Wait...</h2>
      </Modal>
    );
  }
}

export default ModalWaiting;