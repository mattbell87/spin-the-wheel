/**
 * Calulates angular velocity and friction
 */
class Velocity {
    constructor(angles) {
        this.value = 0;
        this.angles = angles || [];
        this.updateListeners = [];
        this.animation = null;
        if (this.angles.length > 1){
            this._dropOldData();
            this.calulate();
        }
        this.maximum = null;
    }

    /**
     * Add an angle for velocity calculation
     * @param {degrees} angle 
     */
    add(angle) {
        this.angles.push(angle);
        this._dropOldData();
        this.calulate();
    }

    /**
     * Set the velocity instantly
     * @param {float} velocity 
     */
    set(velocity) {
        this.value = velocity;
        this._updated();
    }

    /**
     * Clear all the angle values
     */
    clear() {
        this.angles = [];
    }

    /**
     * Calculate the average anglular velocity from the stored values
     */
    calulate() {
        var totalDistance = 0;
        
        //If there is not enough data, return 0
        if (this.angles.length < 2) {
            this.value = 0;
            return;
        }

        //Add the differences
        for (var i = 1; i < this.angles.length; i++) {
            totalDistance += this.angles[i] - this.angles[i-1];
        }

        this.value = totalDistance / (this.angles.length - 1);
        this._updated();
    }

    /**
     * Give the velocity friction
     * @param {float} friction 
     * @param {callback} onZero Callback for when the velocity reaches 0 
     */
    friction(friction, onZero) {
        this._lastFrame = new Date();
        if (this.animation != null) {
            cancelAnimationFrame(this.animation);
            this.animation = null;
        }
        this.animation = requestAnimationFrame(() => this._addFriction(friction, onZero));
    }

    /**
     * Add a listener for when the velocity is being updated
     * @param {callback} listener Listener callback 
     */
    onUpdate(listener) {
        this.updateListeners.push( listener );
    }

    /**
     * Get the speed from the velocity
     * @returns {int}
     */
    getSpeed() {
        var speed = this.value;
        if (speed < 0) {
            speed *= -1;
        }
        return speed;
    }

    /**
     * Perform some operations when the value has been updated
     * @private
     */
    _updated() {
        this.directionForward = this.value > 0;
        if (this.maximum !== null) {
            if (this.value > this.maximum) {
                this.value = this.maximum;
            } else if (this.value < this.maximum * -1) {
                this.value = this.maximum * -1;
            }
        }
        for (var i in this.updateListeners) {
            if (typeof this.updateListeners[i] === 'function') {
                this.updateListeners[i]();
            }
        }
    }

    /**
     * Add friction on every frame
     * @param {float} friction Friction to add 
     * @param {callback} onZero Callback to fire when it reaches 0 
     * @private
     */
    _addFriction(friction, onZero) {
        var currentFrame = new Date();
        var delta = (currentFrame - this._lastFrame) / 1000;
        this._lastFrame = currentFrame;
        if (this.directionForward) {
            this.value -= friction * delta;
        } else {
            this.value += friction * delta;
        }
        var stillMovingForward = (this.directionForward && this.value > 0);
        var stillMovingBackward = (!this.directionForward && this.value < 0);
        if (stillMovingForward || stillMovingBackward) {
            this.animation = requestAnimationFrame(() => this._addFriction(friction, onZero));
        } else {
            this.value = 0;
            if (typeof onZero === "function") {
                onZero();
                this.clear();
                this.animation = null;
            }
        }
        this._updated();
    }

    /**
     * Drop data from the start of the angles array if it's over 3 values long
     * @private
     */
    _dropOldData() {
        while (this.angles.length > 3) {
            this.angles.shift();
        }
    }
}

export default Velocity;
