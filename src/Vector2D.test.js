import Vector2D from './Vector2D';

test('Vector2D can be created without values', ()=>{
    var test = new Vector2D();
    expect(test.x).toEqual(0);
    expect(test.y).toEqual(0);
});

test('Vector2D can be created with values', ()=>{
    var test = new Vector2D(25, 36);
    expect(test.x).toEqual(25);
    expect(test.y).toEqual(36);
});

test('Distance between two Vector2D\'s on the X plane', ()=>{
    var testA = new Vector2D(25, 10);
    var testB = new Vector2D(35, 10);
    expect(testA.distance(testB)).toEqual(10);
});

test('Distance between two Vector2D\'s on the Y plane', ()=>{
    var testA = new Vector2D(10, 60);
    var testB = new Vector2D(10, 35);
    expect(testA.distance(testB)).toEqual(25);
});

test('Vector2D\'s conform to the distance formula', ()=>{
    var testA = new Vector2D(10, 10);
    var testB = new Vector2D(20, 20);
    expect(testA.distance(testB)).toEqual(
        Math.sqrt(Math.pow(10 - 20, 2) + Math.pow(10 - 20, 2))
    );
});

test('Vector2D\'s properly calculate look rotations', ()=>{
    var testA = new Vector2D(0, 0);
    var testB = new Vector2D(-10, 10);
    expect(testA.getLookRotation(testB)).toEqual(45);
});


test('Vector2D\'s throw an error when odd values are passed in', ()=>{
    expect(() => {
        new Vector2D([], "string")
    }).toThrow();
});