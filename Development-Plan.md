# Development plan for Spin The Wheel

## Objectives

As outlined in https://github.com/HIVERY/spin_the_wheel

* allow the user to spin a wheel
* display the results of the last 5 spins
* wheel contains the numbers 1 to 10, in any order
* to spin the wheel, the user can drag the mouse, or use a touchscreen gesture

To reduce some setup, and ensure I can work with ES6 from the get go without worrying about browser support I have chosen to develop this as a ReactJS app.

## Goals

1. Create a ReactJS app
2. Develop SVG Images for the wheel and marker
3. Add a React component for the wheel
4. Create a Vector2D class
5. Vector2D will be able to calculate distance from another Vector2D
6. Add some mouse functionality for the wheel (touchdown / mousedown)
7. Create a Velocity class
8. Configure spin the wheel svg in the component to rotate towards the mouse. Rotation offset will be current rotation. Position offset will be the middle.
9. Spin the wheel class will listen for touchdown / mousedown and add Vector2D values that contain the mouse coordinates to the Velocity class
10. Velocity class clears on touchup / mouseup then calculates and returns velocity.
11. Velocity class will store maximum of 5 coordinates
12. Wheel is spun at velocity
13. Wheel velocity decreases over time, calculated against browser FPS 
14. Develop a map / function / component for the rotation to value conversion.
15. Create a Game component - this will track score and turns
16. When wheel velocity reaches 0 convert the rotation to a value. A turn is taken.
17. Lock the mouse events when wheel velocity is not 0 (if !spinning)
18. Create end game screen when the turn count reaches reach 5
